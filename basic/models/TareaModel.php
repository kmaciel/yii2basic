<?php

namespace app\models;

use yii\base\Model;

class TareaModel extends Model
{
    public $nombre;
    public $correo;
    public $titulo;
    public $mensaje;

    public function rules()
    {
        return [
            [['nombre', 'correo','titulo','mensaje'], 'required'],
            ['correo', 'email'],

        ];
    }
}

?>
