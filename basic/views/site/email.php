<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php  $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'correo') ?>

    <?= $form->field($model, 'titulo') ?>

    <?= $form->field($model, 'mensaje') ?>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>

    </div>

<?php ActiveForm::end(); ?>
