<?php
use yii\helpers\Html;
?>
<p>Su correo se ha enviado con exito:</p>

<ul>
    <li><label>Nombre</label>: <?= Html::encode($model->nombre) ?></li>
    <li><label>Correo</label>: <?= Html::encode($model->correo) ?></li>
    <li><label>Titulo</label>: <?= Html::encode($model->titulo) ?></li>
    <li><label>Mensaje</label>: <?= Html::encode($model->mensaje) ?></li>


</ul>
